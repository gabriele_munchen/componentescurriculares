<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="style.css" rel="stylesheet">
    <title>Netflix</title>
</head>

<body>

    <h2>________________________________________________________________</h2>
    <h2>Número de filmes por ano</h2>
    <!--Número de filmes por ano-->
    <?php
    include_once "conexao.php";
    $query1 = "SELECT shows.release_year, COUNT(shows.release_year) FROM shows WHERE shows.type = 'Movie' GROUP BY shows.release_year";
    $resultado1 = mysqli_query($conn, $query1);
    ?>

    <div class="centralizar">
        <table border="1">
            <?php
            while ($linha = mysqli_fetch_assoc($resultado1)) {
                echo "<tr>";
                echo "<td>" . $linha['release_year'] . "</td>";
                echo "<td>" . $linha['COUNT(shows.release_year)'] . "</td>";
            }
            echo "</tr>";
            ?>
        </table>
    </div>
    </br>
    <h2>________________________________________________________________</h2>

    <h2>Número de séries por duração</h2>
    <!--Número de séries por duração-->
    <?php
    $query2 = "SELECT shows.duration, COUNT(shows.duration) FROM shows WHERE shows.type = 'TV Show' GROUP BY shows.duration";
    $resultado2 = mysqli_query($conn, $query2);
    ?>

    <div class="centralizar">
        <table border="1">
            <?php
            while ($linha = mysqli_fetch_assoc($resultado2)) {
                echo "<tr>";
                echo "<td>" . $linha['duration'] . "</td>";
                echo "<td>" . $linha['COUNT(shows.duration)'] . "</td>";
            }
            echo "</tr>";
            ?>
        </table>
    </div>
    </br>
    <h2>________________________________________________________________</h2>

    <h2>Nome de todos os filmes de Terror</h2>
    <!--nome de todos os filmes de Terror-->
    <?php
    $query3 = 
    "SELECT shows.title 
    FROM shows 
    WHERE shows.listed_in LIKE '%Horror Movies%'";
    $resultado3 = mysqli_query($conn, $query3);
    ?>

    <div class="centralizar">
        <table border="1">
            <?php
            while ($linha = mysqli_fetch_assoc($resultado3)) {
                echo "<tr>";
                echo "<td>" . $linha['title'] . "</td>";
            }
            echo "</tr>";
            ?>
        </table>
    </div>
</body>

</html>